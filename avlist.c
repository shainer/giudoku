/*
 * Giudoku
 * avlist.c: Manage lists for every cell.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>

#include "avlist.h"
#include "sudoku.h"

bool_t create_avlists(sudoku_t ptr)
{
	coord_t i, j;

	for (i = 0; i < COUNT; ++i) {
		for (j = 0; j < COUNT; ++j) {
			if (ptr[i][j].value == 0) {
				if (create_single_avlist(ptr, i, j) == false)
					return false;
			}
		}
	}

	return true;
}

bool_t update_avlists(sudoku_t ptr)
{
	coord_t i, j;

	for (i = 0; i < COUNT; ++i) {
		for (j = 0; j < COUNT; ++j) {
			if (ptr[i][j].value == 0) {
				if (update_single_avlist(ptr, i, j) == false)
					return false;
			}
		}
	}

	return true;
}

bool_t create_single_avlist(sudoku_t ptr, coord_t row, coord_t col)
{
	avlist_t list;
	coord_t n;

	list = (avlist_t) malloc(sizeof(int) * 10);

	if (list == NULL) {
		return false;
	}

	for (n = 0; n < 9; ++n)
		list[n] = (n + 1);

	list[n] = 0;
	ptr[row][col].available = list;
	return true;
}

bool_t is_in_avlist (avlist_t av, int num)
{
	int i;

	for (i = 0; av[i] != 0; ++i) {
		if (av[i] == (unsigned int)num)
			return true;
	}

	return false;
}

void delete_from_avlist(avlist_t ptr, value_t v)
{
	coord_t i = 0;

	while ((ptr[i] != 0) && (ptr[i] != v)) {
		++i;
	}

	while (ptr[i] != 0) {
		ptr[i] = ptr[i + 1];
		++i;
	}
}

int n_elements_avlist (avlist_t cl)
{
	coord_t n;

	for (n = 0; cl[n] != 0; ++n);
	return n;
}

void check_singleness(sudoku_t sk, coord_t row, coord_t col)
{
	avlist_t clist = sk[row][col].available;

	if (n_elements_avlist(clist) == 1) {
		canary = true;
		sk[row][col].value = *clist;
		free(clist);
		update_avlists(sk);
	}
}

bool_t update_single_avlist(sudoku_t ptr, coord_t row, coord_t col)
{
	avlist_t list = ptr[row][col].available;
	block_t rowp;
	block_t colp;
	block_t sqrp;
	coord_t i;

	get_sudoku_row(ptr, row, rowp);
	get_sudoku_column(ptr, col, colp);
	get_sudoku_square(ptr, row, col, sqrp);

	for (i = 0; list[i] != 0; ++i) {
		bool_t in_row = find_in_block(rowp, list[i]);
		bool_t in_col = find_in_block(colp, list[i]);
		bool_t in_sqr = find_in_block(sqrp, list[i]);

		if (in_row || in_col || in_sqr) {
			delete_from_avlist(list, list[i]);
			--i;
		}
	}
	check_singleness(ptr, row, col);

	return true;
}
