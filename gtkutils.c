/*
 * Giudoku
 * gtkutils.c: Useful GTK+ functions
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "gtkutils.h"
#include "gsudoku.h"

/* 
 * sk_create_menu()
 * Create the main Menubar with all its items
 */
GtkWidget *sk_create_menu()
{
	GtkWidget *tmpMenu;
	GtkWidget *fMenu, *fSubmenu, *fItem;
	GtkWidget *hMenu, *hSubmenu, *hItem;

	tmpMenu = gtk_menu_bar_new();

	fMenu = gtk_menu_item_new_with_mnemonic ("_File");
	gtk_menu_shell_append (GTK_MENU_SHELL (tmpMenu), fMenu);
	fSubmenu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (fMenu), fSubmenu);

	fItem = gtk_image_menu_item_new_from_stock (GTK_STOCK_NEW, NULL);
	g_signal_connect(G_OBJECT(fItem), "activate", G_CALLBACK(write_new_sudoku), NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (fSubmenu), fItem);
    
	fItem = gtk_menu_item_new_with_mnemonic("_Importa da file");
	gtk_menu_shell_append(GTK_MENU_SHELL (fSubmenu), fItem);
	g_signal_connect(G_OBJECT(fItem), "activate", G_CALLBACK(import_sudoku), NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (fSubmenu), gtk_separator_menu_item_new());

	fItem = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, NULL);
	g_signal_connect (G_OBJECT (fItem), "activate", gtk_main_quit, NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (fSubmenu), fItem);

	hMenu = gtk_menu_item_new_with_mnemonic ("_Aiuto");
	gtk_menu_shell_append (GTK_MENU_SHELL (tmpMenu), hMenu);
	hSubmenu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (hMenu), hSubmenu);

	hItem = gtk_image_menu_item_new_from_stock (GTK_STOCK_ABOUT, NULL);
	g_signal_connect (G_OBJECT (hItem), "activate", G_CALLBACK(sk_show_dialog), "INFOS Giudoku 0.2\n\nDeveloped by shainer and jmc under GPLv3 license");
	gtk_menu_shell_append (GTK_MENU_SHELL (hSubmenu), hItem);

	return tmpMenu;
}

/*
 * sk_show_dialog()
 * Show an info or error dialog.
 * data is organized in this way: "ERROR text" or "INFOS text"
 */
void sk_show_dialog(GtkWidget *w, gint data)
{
	GtkWidget *toplevel = gtk_widget_get_toplevel(w);
	GtkWidget *aDialog;
	char type[6];
	char *text;
	int type_int = 0;
	
	const char *arg = (const char *) data;
	
	sscanf(arg, "%5s", type); /* saves the first word in type */
	arg += 6; /* going forth a little */
	text = strdup(arg); /* copies the rest of the string */
	
	
	if (strcmp(type, "INFOS") == 0) {
		type_int = GTK_MESSAGE_INFO;
	} else if (strcmp(type, "ERROR") == 0) {
		type_int = GTK_MESSAGE_ERROR;
	}
	
	if (!GTK_WIDGET_TOPLEVEL(toplevel))
	{
		toplevel = NULL;
	}

	/* Creating the dialog */
	aDialog = gtk_message_dialog_new(GTK_WINDOW(toplevel), GTK_DIALOG_DESTROY_WITH_PARENT, type_int, GTK_BUTTONS_CLOSE, (const gchar *) text);
	gtk_dialog_run(GTK_DIALOG(aDialog));
	gtk_widget_destroy(aDialog);
}
