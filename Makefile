.PHONY: all clean

EXECUTABLE = ./giudoku
OBJECTS = main.o sudoku.o avlist.o utility.o gtkutils.o

CFLAGS = -g -W -Wall -pedantic -O3
LIBS =

CC = clang
RM = rm -f

all: $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXECUTABLE) $(LIBS) `pkg-config --cflags --libs gtk+-2.0`

clean:
	$(RM) $(OBJECTS) $(EXECUTABLE)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@ `pkg-config --cflags --libs gtk+-2.0`

#
# Code::Blocks compatibility
#

Debug: all
cleanDebug: clean Debug

Release: all
cleanRelease: clean Release
