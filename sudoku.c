/*
 * Giudoku
 * sudoku.c: Sudoku utilities and solving functions.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "sudoku.h"
#include "avlist.h"
#include "utility.h"
#include "gtkutils.h"

/*
 * Global macros
 */
#define ROW_BYTES    (COUNT * sizeof(cell_t))
#define SUDOKU_BYTES (COUNT * sizeof(cell_t *))

#define SUDOKU_ROW	0
#define SUDOKU_COLUMN   1
#define SUDOKU_SQUARE   2

/*
 * Global definitions
 */
const bool_t true = 1;
const bool_t false = 0;
bool_t canary = 0;

/*
 * Local definitions
 */
bool_t fill_sudoku(sudoku_t);

/*
 * Implementation
 */

/*
 * new_sudoku()
 * Creates a new Sudoku matrix on the heap, zero-ing all of its memory.
 * Returns a sudoku_t (to be freed on exit) on success, NULL on failure.
 */
sudoku_t new_sudoku()
{
	sudoku_t ptr = (sudoku_t) malloc(SUDOKU_BYTES);

	if (ptr == NULL) {
		return NULL;
	}

	if (!fill_sudoku(ptr)) {
		return NULL;
	}

	return ptr;
}

/*
 * fill_sudoku()
 * Allocates memory for COUNT rows of the Sudoku.
 * Returns true on success, false on failure.
 */
bool_t fill_sudoku(sudoku_t ptr)
{
	size_t i;

	for (i = 0; i < COUNT; ++i) {
		ptr[i] = (cell_t *) malloc(ROW_BYTES);

		if (ptr[i] == NULL) {
			delete_sudoku(ptr);
			return false;
		}

		memset(ptr[i], 0, ROW_BYTES);
	}

	return true;
}

/*
 * delete_sudoku()
 * Frees the memory on the heap. Rows can be partially allocated, while
 * the sudoku_t space has to be allocated prior to calling this routine.
 * Returns true.
 */
bool_t delete_sudoku(sudoku_t ptr)
{
	size_t i;

	for (i = 0; i < COUNT; ++i) {
		if (ptr[i] != NULL)
			free(ptr[i]);
	}

	free(ptr);
	return true;
}

/*
 * get_rcs()
 * Writes a line
 * type  - is it a row, a column or a square?
 * x, y  - the coordinates of the cell we want to examinate
 * array - the destination
 */
void get_rcs (sudoku_t sk, cell_t *array[], int type, coord_t y, coord_t x)
{
	coord_t i;
	coord_t by = (y - (y % 3));
	coord_t j, bx = (x - (x % 3));
	coord_t k = 0;

	switch (type) {

		case SUDOKU_ROW:
			for (i = 0; i < COUNT; ++i)
				array[i] = &(sk[y][i]);
			break;

		case SUDOKU_COLUMN:
			for (i = 0; i < COUNT; ++i)
				array[i] = &(sk[i][x]);
			break;

		case SUDOKU_SQUARE:
			for (i = by; i < (by + 3); ++i) {
				for (j = bx; j < (bx + 3); ++j)
					array[k++] = &(sk[i][j]);
			}
			break;

	}
}

bool_t IsResolved(sudoku_t sk)
{
	coord_t y;
	coord_t x;

	for (y = 0; y < COUNT; ++y) {
		for (x = 0; x < COUNT; ++x) {
			if (sk[y][x].value == 0)
				return false;
		}
	}

	return true;
}

/*
 * solve_sudoku()
 * Try to solve the current sudoku
 * Returns true on success, false on failure.
 */
bool_t solve_sudoku(sudoku_t ptr)
{
	coord_t row_num;
	coord_t col_num;

	cell_t *line[9];

	int i;
	int n_occorrenze;

	if (create_avlists(ptr) == false) {
		sk_show_dialog(NULL, TEXT("ERROR Could not initialize lists"));
		return false;
	}
	canary = 0;

	while (IsResolved(ptr) == false) {

		if (update_avlists(ptr) == false) {
			sk_show_dialog(NULL, TEXT("ERROR Could not update lists"));
			return false;
		}

		/* Prima riga */
		for (row_num = 0; row_num < COUNT; ++row_num) {
			get_rcs (ptr, line, SUDOKU_ROW, row_num, 0);
			for (i = 1; i <= 9; ++i) {
				n_occorrenze = count_in_line (line, i);

				if (n_occorrenze == 1) {
					canary = 1;
					find_in_line (line, i);
					update_avlists(ptr);
				}
			}
		}

		for (col_num = 0; col_num < COUNT; ++col_num) {
			get_rcs(ptr, line, SUDOKU_COLUMN, 0, col_num);
			for (i = 1; i <= 9; ++i) {
				n_occorrenze = count_in_line (line, i);

				if (n_occorrenze == 1) {
					canary = 1;
					find_in_line(line, i);
					update_avlists(ptr);
				}
			}
		}

		for (row_num = 0; row_num < COUNT; row_num += 3) {
			for (col_num = 0; col_num < COUNT; col_num += 3) {
				get_rcs(ptr, line, SUDOKU_SQUARE, row_num, col_num);

				for (i = 1; i <= 9; ++i) {
					n_occorrenze = count_in_line (line, i);

					if (n_occorrenze == 1) {
						canary = 1;
						find_in_line(line, i);
						update_avlists(ptr);
					}
				}
			}
		}


		if (canary == 0) {
			return false;
		}
		canary = 0;
	}

	return true;
}
