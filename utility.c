/*
 * Giudoku
 * utility.c: Useful sudoku functions.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utility.h"
#include <stdlib.h>

void get_sudoku_row(sudoku_t ptr, coord_t y, block_t row)
{
  coord_t x;

  for (x = 0; x < COUNT; ++x) {
    row[x] = ptr[y][x].value;
  }
}

void get_sudoku_column(sudoku_t ptr, coord_t x, block_t col)
{
  coord_t y;

  for (y = 0; y < COUNT; ++y) {
    col[y] = ptr[y][x].value;
  }
}

void get_sudoku_square(sudoku_t ptr, coord_t y, coord_t x, block_t square)
{
  coord_t i, by = (y - (y % 3));
  coord_t j, bx = (x - (x % 3));
  coord_t k = 0;

  for (i = by; i < (by + 3); ++i)
    for (j = bx; j < (bx + 3); ++j) {
      square[k++] = ptr[i][j].value;
  }
}

bool_t find_in_block(block_t ptr, value_t v)
{
  coord_t x;

  for (x = 0; x < COUNT; ++x) {
    if (ptr[x] == v)
      return true;
  }

  return false;
}

int count_in_line (cell_t *array[], int i)
{
  int counter = 0;
  coord_t nav;

  for (nav = 0; nav < COUNT; ++nav) {
    if (array[nav]->value == 0 && (is_in_avlist(array[nav]->available, i) == true))
      counter++;
  }

  return counter;
}

void find_in_line (cell_t *array[], int i)
{
  coord_t nav;

  for (nav = 0; nav < COUNT; ++nav) {
    if (array[nav]->value == 0 && (is_in_avlist(array[nav]->available, i) == true)) {
      array[nav]->value = i;
      free(array[nav]->available);
    }
  }
}

