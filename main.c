/*
 * Giudoku
 * main.c: Start point
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "sudoku.h"
#include "avlist.h"
#include "gtkutils.h"
#include "gsudoku.h"

static GtkWidget *window1;
static GtkWidget *entryBox[9][9];
static sudoku_t sudoku;

int main(int argc, char *argv[])
{
	/* Containers */
	GtkWidget *vbox1;
	GtkWidget *table1;
	GtkWidget *frame;
	
	/* Other widgets */
	GtkWidget *entry;
	GtkWidget *sk_Menubar;
	GtkWidget *button;
	
	int i, j;
  	
	gtk_init(&argc, &argv);
	sudoku = new_sudoku(); /* allocating the necessary space */
	
	window1 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window1), "Giudoku v0.2");
	gtk_window_set_position(GTK_WINDOW(window1), GTK_WIN_POS_CENTER);
	g_signal_connect (G_OBJECT (window1), "destroy", gtk_main_quit, NULL);
    
	vbox1 = gtk_vbox_new(FALSE, 6);
	gtk_container_add(GTK_CONTAINER(window1), vbox1);
    
  sk_Menubar = sk_create_menu();
  gtk_box_pack_start(GTK_BOX(vbox1), sk_Menubar, FALSE, FALSE, 0);
    
	frame = gtk_frame_new("Sudoku");
	gtk_container_add(GTK_CONTAINER(vbox1), frame);
	
	table1 = gtk_table_new(10, 9, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE(table1), 3);
	gtk_table_set_col_spacings(GTK_TABLE(table1), 3);
	gtk_container_add(GTK_CONTAINER(frame), table1);
	
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 9; ++j) {
			if ((j + 1) % 3 == 0)
				gtk_table_set_col_spacing(GTK_TABLE(table1), j, 10);
			if ((i + 1) % 3 == 0)
				gtk_table_set_row_spacing(GTK_TABLE(table1), i, 10);
			
			entry = gtk_entry_new_with_max_length(1);
			gtk_entry_set_width_chars(GTK_ENTRY(entry), 2);
			gtk_table_attach_defaults(GTK_TABLE(table1), entry, i, i+1, j, j+1);
			entryBox[i][j] = entry;
		}
	}
	
	button = gtk_button_new_with_label("Solve it!");
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(prepare_solving), NULL);
	gtk_table_attach_defaults(GTK_TABLE(table1), button, 0, 3, 9, 10);
  	
  gtk_widget_show_all (window1);
  gtk_main();
  return 0;
}

/* 
 * write_new_sudoku()
 * Correctly allocates space for a new sudoku,
 * otherwise Mr.Segfault will be hunting me forever.
 */
void write_new_sudoku(GtkWidget *UNUSED(wg), gint UNUSED(data))
{
	int i, j;
	delete_sudoku(sudoku); /* Deleting the previous one */
	sudoku = new_sudoku();
	
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 9; ++j) {
			gtk_entry_set_text(GTK_ENTRY(entryBox[i][j]), "");
		}
	}
}

/*
 * import_sudoku()
 * Import a sudoku from a file directly to the window
 */
void import_sudoku(GtkWidget *UNUSED(wg), gint UNUSED(data))
{
	GtkWidget *dialog;
	const char *skFile;
	
	/* Prepare the dialog for choosing a file */
	dialog = gtk_file_chooser_dialog_new ("Open sudoku from file", GTK_WINDOW(window1), GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
    	skFile = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if (read_sudoku_from_file(skFile) == FALSE) {
			sk_show_dialog(window1, TEXT("ERROR Unable to open or read file"));
		}
	}
		
	gtk_widget_destroy (dialog);
}

/*
 * read_sudoku_from_file()
 * Reads a sequence of 81 unsigned numbers from the given file name
 * and writes it in the window.
 * Zero indicates a free cell.
 * Returns true on success, false on failure.
 */
bool_t read_sudoku_from_file(const char *file_name)
{
 	FILE *fp;
 	bool_t rc = false;

	fp = fopen(file_name, "r");

	if (fp != NULL) {
		rc |= read_sudoku(fp);
    fclose(fp);
  }

	return rc;
}

/*
 * read_sudoku()
 * Reads a sequence of 81 unsigned numbers from the given file stream
 * fp and writes it to the window.
 * Zero indicates a free cell.
 * Returns true on success, false on failure.
 */
bool_t read_sudoku(FILE *fp)
{
	coord_t i, j;

	for (i = 0; i < COUNT; ++i) {
   		for (j = 0; j < COUNT; ++j) {
      		value_t v;
      		char number[2] = {'0', '\0'};

      		fscanf(fp, "%u", &v);

      		if (v > 9)
        		return false;
        	
			number[0] = (int)v + '0';
      		gtk_entry_set_text(GTK_ENTRY(entryBox[i][j]), (const gchar *)number);
    	}
  	}

  	return true;
}

/*
 * input_from_window()
 * Reads what it's written in the entries and store the values in the current sudoku pointer.
 * Zero indicates a free cell.
 */
void input_from_window()
{
	int i, j;
	const gchar *sText;
	
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 9; ++j) {
			sText = gtk_entry_get_text(GTK_ENTRY(entryBox[i][j]));
			if (strcmp(sText, "") == 0)
				sudoku[j][i].value = 0;
			else
				sudoku[j][i].value = atoi(sText);
		}
	}
}

/*
 * print_result()
 * Print the result on screen.
 */
void print_result()
{
	int i, j;
	char number[2] = {'0', '\0'};
	
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 9; ++j) {
			number[0] = (sudoku[j][i].value + '0');
			gtk_entry_set_text(GTK_ENTRY(entryBox[i][j]), (const gchar *)number);
		}
	}
}

/*
 * prepare_solving()
 * Just a wrapper before the sudoku is actually solved and printed.
 */
void prepare_solving(GtkWidget *UNUSED(wg), gint UNUSED(data))
{
	input_from_window();
	
	if (!solve_sudoku(sudoku)) {
		sk_show_dialog(window1, TEXT("ERROR The sudoku can't be solved"));
	}
	
	print_result();
}

