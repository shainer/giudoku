/*
 * Giudoku
 * gsudoku.h: Functions that stand between the graphical parts and the sudoku algorithm below.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GSUDOKU_INCLUDED
#define _GSUDOKU_INCLUDED

#include "types.h"

void input_from_window();
void print_result();
void prepare_solving(GtkWidget *, gint);
void write_new_sudoku(GtkWidget *, gint);
void import_sudoku(GtkWidget *, gint);
bool_t read_sudoku(FILE *);
bool_t read_sudoku_from_file(const char *);

#endif
