/*
 * Giudoku
 * utility.h: Definitions for useful sudoku functions.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILITY_H_INCLUDED
#define UTILITY_H_INCLUDED

#include "sudoku.h"
#include "avlist.h"

void get_sudoku_row(sudoku_t, coord_t, block_t);
void get_sudoku_column(sudoku_t, coord_t, block_t);
void get_sudoku_square(sudoku_t, coord_t, coord_t, block_t);

bool_t find_in_block(block_t, value_t);

int count_in_line (cell_t *array[], int);
void find_in_line (cell_t *array[], int);

#endif
