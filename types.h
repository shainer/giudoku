/*
 * Giudoku
 * types.h: Some customized data types.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

typedef unsigned int value_t;
typedef value_t *avlist_t;

struct _p_cell_t
{
  value_t value;
  avlist_t available;
};

typedef struct _p_cell_t cell_t;
typedef cell_t *row_t;
typedef row_t *sudoku_t;

#define BLOCK_SIZE 9
typedef value_t block_t[BLOCK_SIZE];

typedef unsigned coord_t;
typedef unsigned short ushort_t;

typedef unsigned bool_t;
extern const bool_t true;
extern const bool_t false;

#endif
