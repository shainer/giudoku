/*
 * Giudoku
 * avlist.h: Avlists definitions.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVLIST_H_INCLUDED
#define AVLIST_H_INCLUDED

#include "types.h"

bool_t create_avlists(sudoku_t);
bool_t create_single_avlist(sudoku_t, coord_t, coord_t);

bool_t is_in_avlist (avlist_t, int);

void empty_avlist(avlist_t);
void delete_from_avlist(avlist_t, value_t);

bool_t update_avlists(sudoku_t);
bool_t update_single_avlist(sudoku_t, coord_t, coord_t);

#endif
