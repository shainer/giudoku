/*
 * Giudoku
 * sudoku.h: Definitions of main sudoku functions.
 * Copyright (C) 2009 Lisa Vitolo <syn.shainer@gmail.com>
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SUDOKU_H_INCLUDED
#define SUDOKU_H_INCLUDED

#include <stdio.h>
#include "types.h"

#define COUNT        9

sudoku_t new_sudoku();
bool_t delete_sudoku(sudoku_t);

bool_t find_in_block(block_t, value_t);
void get_sudoku_row(sudoku_t, coord_t, block_t);
void get_sudoku_column(sudoku_t, coord_t, block_t);
void get_sudoku_square(sudoku_t, coord_t, coord_t, block_t);

bool_t solve_sudoku(sudoku_t);
extern bool_t canary;

/*
 * Satisfy GCC when ignoring attributes...
 */
#define UNUSED(x) x __attribute__ ((unused))

/*
 * Wrap the pointer...
 */
#define TEXT(x) ((gint) ((gchar *) (x)))

#endif
